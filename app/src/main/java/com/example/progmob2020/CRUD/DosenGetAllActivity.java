package com.example.progmob2020.CRUD;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Adapter.DosenCRUDRecyclerAdapter;
import com.example.progmob2020.Adapter.MahasiswaCRUDRecyclerAdapter;
import com.example.progmob2020.Model.Dosen;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

    public class DosenGetAllActivity extends AppCompatActivity {
        RecyclerView rvMhs;
        DosenCRUDRecyclerAdapter dosenAdapter;
        ProgressDialog pd;
        List<Dosen> dosenList;

        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_dosen_get_all);

            rvMhs = (RecyclerView)findViewById(R.id.rvGetDosenAll);
            pd = new ProgressDialog(this);
            pd.setTitle("Data Loading");
            pd.show();

            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<List<Dosen>> call = service.getDosen("72180221");

            call.enqueue(new Callback<List<Dosen>>() {
                @Override
                public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                    pd.dismiss();
                    dosenList = response.body();
                    dosenAdapter = new DosenCRUDRecyclerAdapter(dosenList);

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( DosenGetAllActivity.this);
                    rvMhs.setLayoutManager(layoutManager);
                    rvMhs.setHasFixedSize(true);
                    rvMhs.setAdapter(dosenAdapter);

                }

                @Override
                public void onFailure(Call<List<Dosen>> call, Throwable t) {
                    Toast.makeText(DosenGetAllActivity.this, "Loading Error",Toast.LENGTH_LONG);
                }
            });
        }
    }