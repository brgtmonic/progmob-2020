package com.example.progmob2020.Adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Model.Dosen;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class DosenCardAdapter extends RecyclerView.Adapter<DosenCardAdapter.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    public DosenCardAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_cardview_d,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen m = dosenList.get(position);
        holder.tvNama.setText(m.getNama());
        holder.tvNik.setText(m.getNik());
        holder.tvNoTelp.setText(m.getNotelp());
    }

    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvNik, tvNoTelp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.txtNama);
            tvNik = itemView.findViewById(R.id.txtNik);
            tvNoTelp = itemView.findViewById(R.id.txNoTelp);
        }
    }
}
