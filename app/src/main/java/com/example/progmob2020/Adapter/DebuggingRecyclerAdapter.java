package com.example.progmob2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Model.MahasiswaDebugging;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class DebuggingRecyclerAdapter extends RecyclerView.Adapter<DebuggingRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<MahasiswaDebugging> mahasiswaDebugging;

    public DebuggingRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaDebugging = new ArrayList<>();
    }

    public List<MahasiswaDebugging> getMahasiswaDebugging() {
        return mahasiswaDebugging;
    }

    public void setMahasiswaDebugging(List<MahasiswaDebugging> mahasiswaDebugging) {
        this.mahasiswaDebugging = mahasiswaDebugging;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MahasiswaDebugging m = mahasiswaDebugging.get(position);

        holder.txNama.setText(m.getNama());
        holder.txNoTelp.setText(m.getNotelp());
        holder.txNim.setText(m.getNim());
    }

    @Override
    public int getItemCount() {
        return mahasiswaDebugging.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txNama, txNim, txNoTelp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txNama = itemView.findViewById(R.id.txtNama);
            txNim = itemView.findViewById(R.id.txNim);
            txNoTelp = itemView.findViewById(R.id.txNoTelp);
        }
    }
}
