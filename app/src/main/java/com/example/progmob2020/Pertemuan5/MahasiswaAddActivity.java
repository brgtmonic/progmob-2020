package com.example.progmob2020.Pertemuan5;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_add);

        final EditText tbhNim = (EditText)findViewById(R.id.EditNim);
        final EditText tbhNama = (EditText)findViewById(R.id.EditNama);
        final EditText tbhAlamat = (EditText)findViewById(R.id.EditAlamat);
        final EditText tbhEmail = (EditText)findViewById(R.id.editEmail);
        Button btnSimpan = (Button)findViewById(R.id.btnSave);
        pd = new ProgressDialog(MahasiswaAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Data Loading");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        tbhNama.getText().toString(),
                        tbhNim.getText().toString(),
                        tbhAlamat.getText().toString(),
                        tbhEmail.getText().toString(),
                        "Kosongin Saja ",
                        "72180221"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "DATA GAGAL DISIMPAN", Toast.LENGTH_LONG).show();
                    }
                });
            }
            });
    }
}