package com.example.progmob2020.Pertemuan5;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);
        final EditText upCari = (EditText)findViewById(R.id.upNimCari);
        final EditText upNama = (EditText)findViewById(R.id.upNama);
        final EditText upNim = (EditText)findViewById(R.id.upNim);
        final EditText upAlamat = (EditText)findViewById(R.id.upAlamat);
        final EditText upEmail = (EditText)findViewById(R.id.upEmail);
        Button btnUpdate = (Button) findViewById(R.id.btnUp);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Now Loading");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> del= service.delete_mhs(upCari.getText().toString(), "72180221"
                );
                del.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(MahasiswaUpdateActivity.this,"Data Berhasil Update", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Error, Mohon Coba Lagi!",Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefaultResult> add= service.add_mhs(upNama.getText().toString(),
                        upNim.getText().toString(), upAlamat.getText().toString(),
                        upEmail.getText().toString(),"Kosongkan Saja","72180221"
                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Berhasil",Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Error",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}