package com.example.progmob2020.Pertemuan5;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.CRUD.DosenGetAllActivity;
import com.example.progmob2020.R;

public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);
        //Button
        Button btnGetDosen =(Button)findViewById(R.id.btnGetDosen);
        Button btnAddDosen = (Button)findViewById(R.id.btnAddDosen);
        Button btnHps = (Button)findViewById(R.id.btnDelDosen);
        Button btnUpdate = (Button)findViewById(R.id.btnUpdateDose);

        //Action
        btnGetDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
            }
        });
        btnHps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenDeleteActivity.class);
                startActivity(intent);
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenUpdateActivity.class);
                startActivity(intent);
            }
        });

        btnAddDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenAddActivity.class);
                startActivity(intent);
            }
        });
    }

}