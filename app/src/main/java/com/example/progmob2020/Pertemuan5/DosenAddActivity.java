package com.example.progmob2020.Pertemuan5;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_add);

        final EditText tbNik = (EditText)findViewById(R.id.editNikD);
        final EditText tbNama = (EditText)findViewById(R.id.editNamaD);
        final EditText tbAlamat = (EditText)findViewById(R.id.editAlamatD);
        final EditText tbEmail = (EditText)findViewById(R.id.editEmailD);
        Button btnSimpan = (Button)findViewById(R.id.btnSaveD);
        pd = new ProgressDialog(DosenAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Data Loading");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dosen(
                        tbNama.getText().toString(),
                        tbNik.getText().toString(),
                        tbAlamat.getText().toString(),
                        tbEmail.getText().toString(),
                        "Kosong",
                        "72180221"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "DATA GAGAL DISIMPAN", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}