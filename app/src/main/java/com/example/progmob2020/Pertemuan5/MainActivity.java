package com.example.progmob2020.Pertemuan5;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.R;

public class MainActivity extends AppCompatActivity {
    String isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //Button
        Button btnMatkul = (Button)findViewById(R.id.btnMatkul);
        Button btnDosen = (Button)findViewById(R.id.btnDosen);
        Button btnMhs = (Button)findViewById(R.id.btnMhs);
        Button btnOut = (Button)findViewById(R.id.btnLogOut);

        //Action
        SharedPreferences pref = MainActivity.this.getSharedPreferences("pref_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        btnOut.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                          startActivity(intent);
                                          isLogin = pref.getString("isLogin", "0");
                                          if (isLogin.equals("0")) {
                                              editor.putString("isLogin", "1");
                                          } else {
                                              editor.putString("isLogin", "0");
                                          }
                                          editor.commit();
                                      }
                                  });

                btnDosen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, MainDosenActivity.class);
                        startActivity(intent);
                    }
                });
                btnMhs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, MainMhsActivity.class);
                        startActivity(intent);
                    }
                });
                btnMatkul.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, MainMatkulActivity.class);
                        startActivity(intent);
                    }
                });
            }
        }
